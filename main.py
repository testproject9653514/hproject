class OpenFile:
    def __init__(self, name):
        self.file = list(map(float, open(name, "r").read().split()))

    def __call__(self, *args, **kwargs):
        return self.file


class CoefAssimetry:
    def __init__(self, selection):
        self.selection = selection
        self.X = sum(self.selection) / len(self.selection)

    def get_central_moment(self, k: int):
        return sum([(self.selection[i] - self.X) ** k for i in range(len(self.selection))]) / len(self.selection)

    def get_standart_deviation(self):
        return (sum([(self.selection[i] - self.X) ** 2 for i in range(len(self.selection))]) / (
                    len(self.selection) - 1))

    def get_coef(self):
        return self.get_central_moment(3) / self.get_standart_deviation() ** (3 / 2)


